let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');
let token = localStorage.getItem('token');

fetch(`https://ancient-refuge-44607.herokuapp.com/api/courses/${courseId}`,{
	method : 'DELETE',
	headers: {
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
console.log(data)
	if(data === true){
		alert('Course has been disabled')
		window.location.replace('./courses.html')
	} else {
		alert("Something went wrong")
	}
})