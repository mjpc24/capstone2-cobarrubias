let token = localStorage.getItem("token");
let adminUser = localStorage.getItem("isAdmin") === "true";
let addButton = document.querySelector("#adminButton");
let cardFooter;
/* This if statement will allo us to show a button fo adding a course/ a button to redirect us to the addCourse 
page if the user is an admin, however if he/she is a guest or a regular user, they shoud not see the button*/
if (adminUser === false || adminUser === null) {
addButton.innerHTML = null;
} else {
addButton.innerHTML = 
	`
		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class="btn btn-block btn-dark">Add Course</a>
		</div>

	`;
}
fetch("https://ancient-refuge-44607.herokuapp.com/api/courses/")
.then((res) => res.json())
.then((data) => {
// a variable that will store the data to be rendered
let courseData;
//if he number of course is less than 1, display no course availbale
if (data.length < 1) {
	courseData = "No courses available";
} else {
	courseData = data
	.map((course) => {
		if (adminUser == false || !adminUser) {
		if (course.isActive === true) {
			cardFooter = `<a href="./course.html?courseId=${course._id}" value={course._id} class="btn btn-dark text-white btn-block select-course">Select Course</a>`;
			return `
				<div class="col-md-6 my-3 container-courses h-100">
					<div class="card h-100">
						<div class="card-boy h-100">
							<h5 class="card-title text-center course-name">${course.name}</h5>
							<p class="card-text text-center course-description">
								${course.description}
							</p>
							<p class="card-text text-right course-price">
								₱ ${course.price}
							</p>
						</div>
						<div class="card-footer">
							${cardFooter}
						</div>
					</div>
				</div>
			`;
		}
		} else {
		if (course.isActive === true) {
			cardFooter = `
					<a href="./editCourse.html?courseId=${course._id}" value={course._id} class="btn btn-primary text-white btn-block editButton course-buttons">Edit
					</a>
					<a href="./course.html?courseId=${course._id}" value={course._id} class="btn btn-success text-white btn-block deleteButton course-buttons">View
					</a>
					<a href="./disable.html?courseId=${course._id}" value={course._id} class="btn btn-warning text-white btn-block deleteButton course-buttons">Disable
					</a>
				`;
		} else {
			cardFooter = `
					<a href="./editCourse.html?courseId=${course._id}" value={course._id} class="btn btn-primary text-white btn-block course-buttons">Edit
					</a>
					<a href="./course.html?courseId=${course._id}" value={course._id} class="btn btn-success text-white btn-block course-buttons">View
					</a>
					<a href="./enable.html?courseId=${course._id}" value={course._id} class="btn btn-info text-white btn-block course-buttons">Enable
					</a>
					`;
		}
		return `
				<div class="col-md-6 my-3 h-100">
					<div class="card h-100">
						<div class="card-boy h-100">
							<h5 class="card-title text-center course-name">${course.name}</h5>
							<p class="card-text text-center course-description">
								${course.description}
							</p>
							<p class="card-text text-right course-price">
								₱ ${course.price}
							</p>
						</div>
						<div class="card-footer ">
							${cardFooter}
						</div>
					</div>
				</div>


			`;
		}

		//since the collection is an array , we can use the join method to indicate the separator for each element
	})
	.join("");
}
let container = document.querySelector("#coursesContainer");
container.innerHTML = courseData;
});
