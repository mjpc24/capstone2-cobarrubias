let params = new URLSearchParams(window.location.search);
let name = document.querySelector("#firstName");
let lname = document.querySelector("#lastName");
let userId =params.get('userId');
let mobile = document.querySelector("#mobileNo");
let cCourse = document.querySelector("#cContainer");
let userCourses = document.querySelector("#courseContainer");
let token = localStorage.getItem('token');

fetch(`https://ancient-refuge-44607.herokuapp.com/api/users/details/`,{
	headers: {
		'Authorization' : `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data =>{
console.log(data)
	if (data) {
		name.value = data.firstName 
		lname.value = data.lastName
		mobile.value = data.mobileNo
		cCourse.innerHTML =
		`
		<a href="./editProfile.html?userId=${data._id}" value={data._id} class="btn btn-dark text-white btn-block editButton">
		Manage Profile
		</a>
		`
	}
	data.enrollments.forEach(course => {
		let courseId = course.courseId
		fetch(`https://ancient-refuge-44607.herokuapp.com/api/courses/${courseId}`,{
			headers: {
				"Content-Type": 'application/json',
				'Authorization': `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data =>{		
			if (data) {
				console.log(data.enrolledOn)
				userCourses.innerHTML +=
				` 
				<div class="card">
					<br>
						<div class="card-body">
							<h5 class="card-title">${data.name}</h5>
							<h6 class="card-subtitle mb-2 text-muted">Date Enrolled: ${course.enrolledOn}</h6>
							<h6 class="card-subtitle mb-2 text-muted">Status: ${course.status}</h6>
						</div>
					<br>
				</div>
				`
			} else {
				alert("Something went wrong");
			}
		})
	})	
})









