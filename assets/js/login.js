let loginForm = document.querySelector("#logInUser");

loginForm.addEventListener("submit", (e)=>{
	e.preventDefault();
	let email = document.querySelector("#userEmail").value
	let password = document.querySelector("#password").value
	if (email == "" || password == "") {
		alert("Please input your email and/or Password")
	} else {
		fetch('https://ancient-refuge-44607.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
		//add if-else statement which will verify our users that they have logged in successfully and got their tokens. If the user cannot log in successfully , thery will get an alert.if the user has logged in successfully, wew ill save his token into the web browser and get his details using fetch.
		//to save data in to the web browser. this storeage is read only,therefore, we set our data into the localstorage through javascript by providing a key/value pair in the storage.
		//The method setItem() for localstorage allows us to save data in to the local storage. syntax below:
		//localStorage.setItem("key", data)
		//To get utem from the localstorage, this is the syntax
		//localStorage.getItem("key")
		if(data.accessToken){
			//set the token into the local storage
			localStorage.setItem('token', data.accessToken);
			//send a fetch request to decode the JWT and obtain the User ID and isAdmin property
			fetch('https://ancient-refuge-44607.herokuapp.com/api/users/details', {
				headers: { 
					Authorization: `Bearer ${data.accessToken}`	
				}
			})
				.then(res => res.json())
				.then(data => {
					localStorage.setItem("id", data._id);
					localStorage.setItem("isAdmin", data.isAdmin);
					
					//window.location.replace = this allows to redirect our user to another page.
					window.location.replace("./courses.html");
				})	
		}else {
			alert("Login Failed.Something went Wrong");
			}
		})
	}
})