let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');
let token = localStorage.getItem('token');
let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDescription")
let coursePrice = document.querySelector("#coursePrice")

fetch(`https://ancient-refuge-44607.herokuapp.com/api/courses/${courseId}`)
	.then(res => res.json())
	.then( data => {	
		courseName.value = data.name
		courseDesc.value = data.description
		coursePrice.value = data.price
	//add place holder
	let formSubmit = document.querySelector("#editCourse")
	formSubmit.addEventListener("submit", (e) => {
	e.preventDefault()
	let courseName = document.querySelector("#courseName").value
	let price = document.querySelector("#coursePrice").value
	let description = document.querySelector("#courseDescription").value
		fetch('https://ancient-refuge-44607.herokuapp.com/api/courses', {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				id: courseId,
				name: courseName,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
		//if the creation of the course is successful, redirect admin to the courses page
			if (data === true) {
				window.location.replace('./courses.html')
			} else {
		//Error while creating a course:
			alert("Course Creation Failed: Something Went Wrong")
			}
		})
	})
})