let params = new URLSearchParams(window.location.search);
let userId = params.get('userId');
let token = localStorage.getItem('token');
let firstName = document.querySelector("#firstName");
let lastName = document.querySelector("#lastName");
let mobileNo = document.querySelector("#mobileNo");

fetch(`https://ancient-refuge-44607.herokuapp.com/api/users/details/${userId}`)
.then(res => res.json())
.then(data => {
	firstName.value = data.firstName
    lastName.value = data.lastName
    mobileNo.value = data.mobileNo
})
document.querySelector("#editProfile").addEventListener("submit", (e) => {
	e.preventDefault()
	let fName = firstName.value
    let lName = lastName.value
    let mNo = mobileNo.value
	let token = localStorage.getItem('token')
	fetch('https://ancient-refuge-44607.herokuapp.com/api/users/update', {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({
            firstName: fName,
            lastName: lName,
            mobileNo: mNo,
        })
    })
    .then(res => res.json())
    .then(data => {
    	//creation of new course successful
    	if(data === true){
    	    //redirect to courses index page
    	    window.location.replace("./profile.html")
    	}else{
    	    //error in creating course, redirect to error page
    	    alert("something went wrong")
    	}
    })
})