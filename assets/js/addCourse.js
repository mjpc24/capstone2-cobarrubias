let formSubmit = document.querySelector("#createCourse");
let courseName = document.querySelector("#courseName")
let description =  document.querySelector("#courseDescription")
let price =  document.querySelector("#coursePrice")

formSubmit.addEventListener("submit",(e) => {
	e.preventDefault()
	if (courseName.value.length === 0 || description.value.length === 0 || price.value.length === 0) {
		alert("All fields are required");
	let nameSpan = courseName.nextElementSibling;
	let descSpan = description.nextElementSibling;
	let priceSpan = price.nextElementSibling;
		if (courseName.value.length === 0){
			nameSpan.classList.remove("d-none");
		} else{
			nameSpan.classList.add("d-none");		
		}
		if (description.value.length === 0) {
			descSpan.classList.remove("d-none");
		} else{
			descSpan.classList.add("d-none");	
		}
		if (price.value.length === 0) {
			priceSpan.classList.remove("d-none");
		} else{
			priceSpan.classList.add("d-none");
		}
	}else{
		let courseName2 = document.querySelector("#courseName").value
		let description2 =  document.querySelector("#courseDescription").value
		let price2=  document.querySelector("#coursePrice").value
		let token = localStorage.getItem("token")
		console.log(token);
		console.log(courseName,description ,price)
		//create afetch request to add a new course:
		fetch('https://ancient-refuge-44607.herokuapp.com/api/courses',{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				name: courseName2,
				description: description2,
				price: price2
			})
		})
		.then(res => res.json())
		.then(data => {
			//if the creation of trhe course is successful,redirect admin to the coureses page
			if(data === true){
			//redirect the admin to the courses page
			alert("New Course has been created");
			window.location.replace("./courses.html");
			} else {
				//error while creating a course
				alert("Course Creation failed. Something went wrong");
			}
		})
	}
})
		


	


