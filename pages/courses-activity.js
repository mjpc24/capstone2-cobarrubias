let token = localStorage.getItem("token");
let adminUser = localStorage.getItem("isAdmin") === "true";
let addButton = document.querySelector("#adminButton");

//This if statement will allo us to show a button fo adding a course/ a button to redirect us to the addCourse page if the user is an admin, however if he/she is a guest or a regular user, they shoud not see the button

if(adminUser === false || adminUser === null){

	addButton.innerHTML = null

} else {

	addButton.innerHTML =`

		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class="btn btn-block btn-primary">Add Course</a>
		</div>

	`
}


fetch('http://localhost:8000/api/courses/')	

.then(res => res.json())
.then(data => {
	console.log(data)		
})	


